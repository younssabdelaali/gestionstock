package com.emsi.ma.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Article")
public class Article implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idArticle  ;
	private String CodeArticle ;
	private String designation;
	private BigDecimal prixUnitiareHt;
	private BigDecimal TauxTva;
	private BigDecimal prixUnitiareTTC;
	private String photo ;
	
	@ManyToOne
	@JoinColumn(name = "idCategory ")
	private  Categorie category;


	
	
	
	public Article(String codeArticle, String designation, BigDecimal prixUnitiareHt, BigDecimal tauxTva,
			BigDecimal prixUnitiareTTC, String photo, Categorie category) {
		super();
		CodeArticle = codeArticle;
		this.designation = designation;
		this.prixUnitiareHt = prixUnitiareHt;
		TauxTva = tauxTva;
		this.prixUnitiareTTC = prixUnitiareTTC;
		this.photo = photo;
		this.category = category;
	}


	public String getCodeArticle() {
		return CodeArticle;
	}


	public void setCodeArticle(String codeArticle) {
		CodeArticle = codeArticle;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public BigDecimal getPrixUnitiareHt() {
		return prixUnitiareHt;
	}


	public void setPrixUnitiareHt(BigDecimal prixUnitiareHt) {
		this.prixUnitiareHt = prixUnitiareHt;
	}


	public BigDecimal getTauxTva() {
		return TauxTva;
	}


	public void setTauxTva(BigDecimal tauxTva) {
		TauxTva = tauxTva;
	}


	public BigDecimal getPrixUnitiareTTC() {
		return prixUnitiareTTC;
	}


	public void setPrixUnitiareTTC(BigDecimal prixUnitiareTTC) {
		this.prixUnitiareTTC = prixUnitiareTTC;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public Categorie getCategory() {
		return category;
	}


	public void setCategory(Categorie category) {
		this.category = category;
	}


	public Article() {
		super();
	}

	
	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
	
	

}
