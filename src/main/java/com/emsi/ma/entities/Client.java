package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name ="client")
public class Client implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idClient  ;
	private String Nom;
	private String Prenom;
	private String adresse;
	private String photo;
	private String email;
	
	@OneToMany(mappedBy = "client" , fetch = FetchType.LAZY)
	private Collection<CommandeClient> commandeClient ;
	

	public Collection<CommandeClient> getCommandeClient() {
		return commandeClient;
	}


	public Client(String nom, String prenom, String adresse, String photo, String email,
			Collection<CommandeClient> commandeClient) {
		super();
		Nom = nom;
		Prenom = prenom;
		this.adresse = adresse;
		this.photo = photo;
		this.email = email;
		this.commandeClient = commandeClient;
	}


	public void setCommandeClient(Collection<CommandeClient> commandeClient) {
		this.commandeClient = commandeClient;
	}


	public Long getIdClient() {
		return idClient;
	}


	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}


	public String getNom() {
		return Nom;
	}


	public void setNom(String nom) {
		Nom = nom;
	}


	public String getPrenom() {
		return Prenom;
	}


	public void setPrenom(String prenom) {
		Prenom = prenom;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Client(String nom, String prenom, String adresse, String photo, String email) {
		super();
		Nom = nom;
		Prenom = prenom;
		this.adresse = adresse;
		this.photo = photo;
		this.email = email;
	}


	public Client() {
		super();
	}

	

	

}
