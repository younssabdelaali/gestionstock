package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "categorie")
public class Categorie implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCategory  ;
	private String CodeCategory;
	private String designation;
	@OneToMany(mappedBy = "category",fetch = FetchType.LAZY)
	private Collection<Article> article ;
	
	

	public Categorie(String codeCategory, String designation, Collection<Article> article) {
		super();
		CodeCategory = codeCategory;
		this.designation = designation;
		this.article = article;
	}


	public Long getIdCategory() {
		return idCategory;
	}


	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}


	public String getCodeCategory() {
		return CodeCategory;
	}


	public void setCodeCategory(String codeCategory) {
		CodeCategory = codeCategory;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public Collection<Article> getArticle() {
		return article;
	}


	public void setArticle(Collection<Article> article) {
		this.article = article;
	}


	public Categorie() {
		super();
	}

	
	public Long getIdArticle() {
		return idCategory;
	}

	public void setIdArticle(Long idArticle) {
		this.idCategory = idArticle;
	}
	
	

}
