package com.emsi.ma.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name ="ligneCMdFornisseur")
public class ligneCmdFornisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idligneCmdFornisseur  ;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article ;
	
	
	@ManyToOne()
	@JoinColumn(name ="idCommandFornisseur")
	private CommmandFornisseur commandefornisseur;

	public ligneCmdFornisseur() {
		super();
	}

	public Long getIdligneCmdFornisseur() {
		return idligneCmdFornisseur;
	}

	public void setIdligneCmdFornisseur(Long idligneCmdFornisseur) {
		this.idligneCmdFornisseur = idligneCmdFornisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommmandFornisseur getCommandefornisseur() {
		return commandefornisseur;
	}

	public void setCommandefornisseur(CommmandFornisseur commandefornisseur) {
		this.commandefornisseur = commandefornisseur;
	}

	
	
	

}
