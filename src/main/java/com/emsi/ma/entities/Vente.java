package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name ="Vente")
public class Vente implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idVente  ;
	
	
	private String code ;
	
	@OneToMany(mappedBy = "vente",fetch = FetchType.LAZY)
	private Collection<LigneDeVente> lignedevente ;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateDeVente;
	
	

	public Vente() {
		super();
	}



	public Long getIdVente() {
		return idVente;
	}



	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public Date getDateDeVente() {
		return dateDeVente;
	}



	public void setDateDeVente(Date dateDeVente) {
		this.dateDeVente = dateDeVente;
	}

	
	
	

}
