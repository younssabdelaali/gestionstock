package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name ="LigneDeCommande")
public class ligneDeCommande implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLigneDeCommande  ;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article ;
	
	@ManyToOne
	@JoinColumn(name = "idCommandCLient ")
	private CommandeClient commandeClient ;
	public ligneDeCommande() {
		super();
	}
	public Long getIdLigneDeCommande() {
		return idLigneDeCommande;
	}
	public void setIdLigneDeCommande(Long idLigneDeCommande) {
		this.idLigneDeCommande = idLigneDeCommande;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public CommandeClient getCommandeClient() {
		return commandeClient;
	}
	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}

	
}
