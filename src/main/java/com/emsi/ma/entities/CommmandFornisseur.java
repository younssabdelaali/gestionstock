package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name ="CommandFornisseur")
public class CommmandFornisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCommandFornisseur  ;
	@Temporal(TemporalType.TIMESTAMP)
	private Date datecommande;
	
	@ManyToOne()
	@JoinColumn(name ="idFornisseur")
	private fornisseur fornisseur;

	@OneToMany(mappedBy = "commandefornisseur",fetch = FetchType.LAZY)
	private Collection<ligneCmdFornisseur> ligneCommandeFornisseur ;
	
	
	public CommmandFornisseur() {
		super();
	}


	public Long getIdCommandFornisseur() {
		return idCommandFornisseur;
	}


	public void setIdCommandFornisseur(Long idCommandFornisseur) {
		this.idCommandFornisseur = idCommandFornisseur;
	}


	public Date getDatecommande() {
		return datecommande;
	}


	public void setDatecommande(Date datecommande) {
		this.datecommande = datecommande;
	}


	public fornisseur getFornisseur() {
		return fornisseur;
	}


	public void setFornisseur(fornisseur fornisseur) {
		this.fornisseur = fornisseur;
	}


	public Collection<ligneCmdFornisseur> getLigneCommandeFornisseur() {
		return ligneCommandeFornisseur;
	}


	public void setLigneCommandeFornisseur(Collection<ligneCmdFornisseur> ligneCommandeFornisseur) {
		this.ligneCommandeFornisseur = ligneCommandeFornisseur;
	}

	

}
