package com.emsi.ma.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name ="MvmDeStock")
public class MvmDeStock implements Serializable{
	
	public static final int Entree =1; 
	
	public static final int sortie =2; 
	
	@Id
	@GeneratedValue
	private Long idMvmStock  ;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvm;
	
	private BigDecimal quantite;
	
	private int typeMvm;
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	public MvmDeStock() {
		super();
	}

	public Long getIdMvmStock() {
		return idMvmStock;
	}

	public void setIdMvmStock(Long idMvmStock) {
		this.idMvmStock = idMvmStock;
	}

	public Date getDateMvm() {
		return dateMvm;
	}

	public void setDateMvm(Date dateMvm) {
		this.dateMvm = dateMvm;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvm() {
		return typeMvm;
	}

	public void setTypeMvm(int typeMvm) {
		this.typeMvm = typeMvm;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	
	
	

}
