package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name ="Fornisseur")	
public class fornisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idFornisseur  ;
	private String Nom;
	private String Prenom;
	private String adresse;
	private String photo;
	private String email;

	
	
	@OneToMany(mappedBy = "fornisseur" ,fetch = FetchType.LAZY)
	private Collection<CommmandFornisseur> commandFornisseur ;
	
	
	public fornisseur() {
		super();
	}

	public fornisseur(String nom, String prenom, String adresse, String photo, String email) {
		super();
		Nom = nom;
		Prenom = prenom;
		this.adresse = adresse;
		this.photo = photo;
		this.email = email;
	}

	public Long getIdFornisseur() {
		return idFornisseur;
	}

	public void setIdFornisseur(Long idFornisseur) {
		this.idFornisseur = idFornisseur;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<CommmandFornisseur> getCommandFornisseur() {
		return commandFornisseur;
	}

	public void setCommandFornisseur(Collection<CommmandFornisseur> commandFornisseur) {
		this.commandFornisseur = commandFornisseur;
	}

	
	
	
	

	
	

}
