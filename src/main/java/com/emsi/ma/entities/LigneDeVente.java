package com.emsi.ma.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity

public class LigneDeVente implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLigneDeVente  ;
	
	
	@ManyToOne()
	@JoinColumn(name = "idArticle")
	private Article article ;
	
	@ManyToOne()
	@JoinColumn(name = "idVente")
	private Vente vente;
	
	public LigneDeVente() {
		super();
	}

	public Long getIdLigneDeVente() {
		return idLigneDeVente;
	}

	public void setIdLigneDeVente(Long idLigneDeVente) {
		this.idLigneDeVente = idLigneDeVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

	

}
