package com.emsi.ma.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name ="CommandeClient")
public class CommandeClient implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCommandCLient  ;
	private String code ;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datecommande;
	
	@ManyToOne()
	@JoinColumn(name ="idClient")
	private Client client ;
	
	@OneToMany(mappedBy = "commandeClient",fetch = FetchType.LAZY)
	private Collection<ligneDeCommande> ligneCommandeClient ;

	public CommandeClient() {
		super();
	}

	public Long getIdCommandCLient() {
		return idCommandCLient;
	}

	public void setIdCommandCLient(Long idCommandCLient) {
		this.idCommandCLient = idCommandCLient;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDatecommande() {
		return datecommande;
	}

	public void setDatecommande(Date datecommande) {
		this.datecommande = datecommande;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Collection<ligneDeCommande> getLigneCommandeClient() {
		return ligneCommandeClient;
	}

	public void setLigneCommandeClient(Collection<ligneDeCommande> ligneCommandeClient) {
		this.ligneCommandeClient = ligneCommandeClient;
	}

	
	
	
	

}
